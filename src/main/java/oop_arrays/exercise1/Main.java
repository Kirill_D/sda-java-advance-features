package oop_arrays.exercise1;

public class Main {
    public static void main(String[] args) {

        int[] numericArr = {4, 6, 13, 56, 32, 40};
        String[] stringArr = {"Watermelon", "Pear", "Banana", "Apple", "Strawberry"};

        SortArray sortArray = new SortArray();

        sortArray.sortNumericArr(numericArr);
        sortArray.sortStringArr(stringArr);

    }
}
